package sheridan;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class MealsServiceTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	@Test
	public void testDrinksRegular() {
		List<String> drinkList=MealsService.getAvailableMealTypes(MealType.DRINKS);
		assertTrue("Test",!drinkList.isEmpty());
	}
	@Test
	public void testDrinksException() {
		List<String> drinkList=MealsService.getAvailableMealTypes(null);
		assertFalse("Test",drinkList.get(0).equalsIgnoreCase("No Brands Available"));
	}
	@Test
	public void testDrinksIn() {
		List<String> drinkList=MealsService.getAvailableMealTypes(MealType.DRINKS);
		assertTrue("Test",drinkList.size()>3);
	}
	@Test
	public void testDrinksout() {
		List<String> drinkList=MealsService.getAvailableMealTypes(null);
		assertTrue("Test",drinkList.size()<2);
	}


}
